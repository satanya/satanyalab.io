---
title: Cramer's Rule
date: 2019-11-05 22:32:36
tags: Math
toc: true
---

> In linear algebra, Cramer's rule is an explicit formula for the solution of a system of linear equations with as many equations as unknowns, valid whenever the system has a unique solution.

<!--more-->

# 

先設個三元一次方程組

$$
\begin{cases}
a_{11} x_1 + a_{12} x_2 + a_{13} x_3 = b_1\\
a_{21} x_1 + a_{22} x_2 + a_{23} x_3 = b_2\\
a_{31} x_1 + a_{32} x_2 + a_{33} x_3 = b_3 
\end{cases}
$$

若係數矩陣$\boldsymbol{A}$爲方陣，如例中

$$
\boldsymbol{A} = 
\begin{bmatrix}
a_{11} & a_{12} & a_{13} \\
a_{21} & a_{22} & a_{23} \\
a_{31} & a_{32} & a_{33}
\end{bmatrix}
$$

當$|\boldsymbol{A}| \neq 0$時，有

$$
x_1 = \frac{|\boldsymbol{A}_1|}{|\boldsymbol{A}|},
x_2 = \frac{|\boldsymbol{A}_2|}{|\boldsymbol{A}|}, 
x_3 = \frac{|\boldsymbol{A}_3|}{|\boldsymbol{A}|}
$$

其中，$\boldsymbol{A}_i$爲$\boldsymbol{A}$之第$i$列用常數項矩陣$\boldsymbol{b} = \begin{bmatrix}b_1 \\b_2 \\b_3\end{bmatrix}$替換而得之，

例如：

$$
\boldsymbol A_2 = 
\begin{bmatrix}
a_{11} & b_{1} & a_{13} \\
a_{21} & b_{2} & a_{23} \\
a_{31} & b_{3} & a_{33}
\end{bmatrix}
$$

一般的，對於任意未知數量等於方程數量的$n$元線性方程組:

$$
\boldsymbol{A_n}
\begin{bmatrix}
x_1 \\
\vdots \\
x_n
\end{bmatrix}
=
\begin{bmatrix}
b_1 \\
\vdots \\
b_n
\end{bmatrix}
$$

當$|\boldsymbol{A}| \ne 0$時，都有$x_i = \frac{|\boldsymbol{A}_i|}{|\boldsymbol{A}|}$

<article class="message is-link">
  <div class="message-body">
  1. 當且僅當未知數矩陣或係數矩陣爲方陣（即未知數量等於方程數量時）方適用
  2. Cramer's Rule計算量過大，不建議於考試中使用，但是電腦運算十分方便
  </div>
</article>

### 定理

- 若齊次n元線性方程組有n個方程，且$\boldsymbol{A} \neq 0$，則該方程組只有零解

- 線性齊次方程組有非零解 $\Leftrightarrow$ $\boldsymbol{A}=0$   *（尚待證明）*
