---
title: 交流的基礎
date: 2019-11-08 19:07:11
tags: Daily
---

只要你不是[言語障礙 (Speech and language impairment) ](https://en.wikipedia.org/wiki/Speech_and_language_impairment)患者，但是如果你想問什麼人值得交流或者容易交流的話，以下兩點是我認為較為重要的兩點，當然兩點是遠不止的。

### 「觀點」與「事實」

儘管二者難以分別，但是這是十分重要的一點，首先對自己而言，自負和短見可能讓你認爲自己持有的觀點是不爭的事實，更甚者，還能形而上學的推演出一系列零自己深信不疑的推論，儘管你的觀點很可能與事實非常貼近，仍可能陷入[滑坡謬誤(SSA)](https://en.wikipedia.org/wiki/Slippery_slope)，於此不贅述後者。

什麼是觀點？這裏引用[Wikipedia的定義](https://en.wikipedia.org/wiki/Point_of_view_(philosophy))

> A **point of view,** in philosophy, is an attitude–how one sees or thinks: a specified (or stated) manner of consideration as in "my personal point of view".

顯然的，「觀點」是主觀的而「事實」是絕對客觀的，因此請不要將自己的觀點認爲是事實，當然你可能認爲這是一句廢話。

### 爭議因何而起

多數人都能程度不同的做到包容異見，但遺憾的是中國向來不能不同價值觀的存在，從教育到輿論皆如此，中國人迷信權威、崇拜權利，這不是中共決定的或造成的，相反，CCP同KMT恰是利用如此，此為拜民族性所賜。因此若能包容異見，那便是極好的。
