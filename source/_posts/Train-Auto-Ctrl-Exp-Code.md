---
title: 列車自動控制實驗工程檔
date: 2019-11-05 20:46:21
tags: Train
---

是爲4次列控實驗之原始碼，目前，左列諸項皆以GOLANG實現

<!--more-->

## 首

[CTCS應答器報文解析](https://gitlab.com/satanya/ctcs-transponder-message-parser)

## 次

暫無待補

## 叄

[RBC用戶機](https://gitlab.com/satanya/rbcclient)

## 肆

[模擬區間移動頻發碼](https://gitlab.com/satanya/simusection)
